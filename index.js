const express = require("express");
const app = express();
const port = 3000;
const userRouter = require("./users/route");

// middleware
app.use(express.json());
app.use(express.static("public"));

app.get("/", (req, res) => {
  return res.sendFile("index.html");
});

app.use("/", userRouter);

app.get("/ping", (req, res) => {
  return res.json("SELAMAT DATANG");
});

app.listen(3000, () => {
  console.log(`Example app listening on port 3000`);
});

const { validationResult } = require("express-validator");
const schemaValidation = async (req, res, next) => {
  const result = await validationResult(req);
  if (result.isEmpty()) {
    next();
  } else return res.send({ error: result.array() });
};

module.exports = schemaValidation;

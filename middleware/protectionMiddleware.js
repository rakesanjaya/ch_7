const protection = async (req, res, next) => {
  const tokenId = req.token.id.toString();

  const { idUser } = await req.params;
  if (tokenId === idUser) {
    next();
  } else {
    return res.status(400).json({ message: "Unauthorized" });
  }
};

module.exports = protection;

const jwt = require("jsonwebtoken");

const authMiddleware = async (req, res, next) => {
  // cek apakah ada authorization didalam header
  // ambil key authorization dalam header
  const { authorization } = req.headers;
  // jika authorization undifined/null maka reject request
  if (authorization === undefined) {
    res.statusCode = 400;
    return res.json({ message: " Unauthorized" });
  }
  try {
    // cek apakah authorization valid, kalau tidak valid return ke Unauthorized, jika valid lanjutkan proses
    const token = await jwt.verify(authorization, "who knows");
    req.token = token;
    next();
  } catch (error) {
    res.statusCode = 400;
    return res.json({ message: "invalid token" });
  }
};

module.exports = authMiddleware;

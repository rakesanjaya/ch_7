"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class gameRoom extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      gameRoom.belongsTo(models.User, {
        foreignKey: "playerOneId",
        as: "playerOne",
      });
      gameRoom.belongsTo(models.User, {
        foreignKey: "playerTwoId",
        as: "playerTwo",
      });
    }
  }
  gameRoom.init(
    {
      playerOneId: DataTypes.INTEGER,
      playerOneChoice: DataTypes.STRING,
      playerOneResult: DataTypes.STRING,
      playerTwoId: DataTypes.INTEGER,
      playerTwoChoice: DataTypes.STRING,
      playerTwoResult: DataTypes.STRING,
      roomStatus: DataTypes.STRING,
      roomName: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "gameRoom",
    }
  );
  return gameRoom;
};

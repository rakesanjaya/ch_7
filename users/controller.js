const UserModel = require("./model");
const jwt = require("jsonwebtoken");
const { validationResult } = require("express-validator");
const { json } = require("sequelize");
const e = require("express");
const { theGameResult, computerChoice } = require("./gameResultValidation");

class UserController {
  getAllUsers = async (req, res) => {
    const allUser = await UserModel.getAllUser();
    return res.json(allUser);
  };
  getSingleUser = async (req, res) => {
    const { idUser } = req.params;
    try {
      const users = await UserModel.getSingleUser(idUser);
      if (users) {
        return res.json(users);
      } else {
        res.statusCode = 400;
        return res.json({ message: "User id tidak ditemukan : " + idUser });
      }
    } catch (error) {
      res.statusCode = 400;
      return res.json({ message: "User id tidak ditemukan : " + idUser });
    }
  };

  getsingleuserbio = async (req, res) => {
    const { idUser } = req.params;
    const findbio = await UserModel.getsingleuserbio(idUser);
    try {
      if (findbio) {
        return res.json(findbio);
      } else {
        res.statusCode = 400;
        return res.json({
          message: "biodata user tidak ditemukan : " + idUser,
        });
      }
    } catch (error) {
      res.statusCode = 400;
      return res.json({ message: "biodata user tidak ditemukan : " + idUser });
    }
  };

  updateUserBio = async (req, res) => {
    // dapatkan id user dari request parameter
    const { idUser } = req.params;

    // dapatkan fullname, PhoneNumber dan Address dari request body
    const { fullname, PhoneNumber, Address } = req.body;

    // update user
    // check apakah user bio sudah ada ?
    const existUser = await UserModel.isUserBioExist(idUser);

    // jika sudah ada update user bio
    if (existUser) {
      UserModel.updateUserBio(idUser, fullname, PhoneNumber, Address);
      // console.log("user bio sudah exist, update ");
    } else {
      // jika belum ada create user bio
      UserModel.createUserBio(idUser, fullname, PhoneNumber, Address);
      // console.log("user bio belum ada, create!");
    }

    return res.json({ message: "user bio is updated" });
  };

  registerUsers = async (req, res) => {
    const dataRequest = req.body;

    //  cek apakah username, email dan password benar
    // if (dataRequest.username === undefined || dataRequest.username === "") {
    //   res.statusCode = 400;
    //   return res.json({ message: "Username is invalid" });
    // }

    // if (dataRequest.email === undefined || dataRequest.email === "") {
    //   res.statusCode = 400;
    //   return res.json({ message: "Email is invalid" });
    // }

    // if (dataRequest.password === undefined || dataRequest.password === "") {
    //   res.statusCode = 400;
    //   return res.json({ message: "Password is invalid" });
    // }
    // if (dataRequest.fullname === undefined || dataRequest.fullname === "") {
    //   res.statusCode = 400;
    //   return res.json({ message: "fullname is invalid" });
    // }

    //   cek apakah username dan email sudah teregistrasi

    const existData = await UserModel.isUserRegistered(dataRequest);
    // data.fullname === dataRequest.fullname

    if (existData) {
      return res.json({ message: "Username or email is exist!" });
    }

    //   record data kedalam daftarUser
    UserModel.recordNewdata(dataRequest);

    res.json({ message: "Sukses menambahkan User baru!!" });
  };

  // manual create userBio
  // registerBio = async (req, res) => {
  //   const dataBio = req.body;
  //   try {
  //     if (dataBio.fullname === undefined || dataBio.fullname === "") {
  //       res.statusCode = 400;
  //       return res.json({ message: "mohon diisi nama lengkap" });
  //     }

  //     if (dataBio.PhoneNumber === undefined || dataBio.PhoneNumber === "") {
  //       res.statusCode = 400;
  //       return res.json({ message: "mohon diisi nomor hp" });
  //     }

  //     if (dataBio.Address === undefined || dataBio.Address === "") {
  //       res.statusCode = 400;
  //       return res.json({ message: "mohon diisi alamat " });
  //     }
  //     if (dataBio.User_id === undefined || dataBio.User_id === "") {
  //       res.statusCode = 400;
  //       return res.json({
  //         message: " mohon diisi dengan id yang telah anda dapatkan ",
  //       });
  //     }
  //   } catch (error) {
  //     console.log(error);
  //   }

  //   //   cek apakah biodata ada yang sama
  //   const existBio = await UserModel.isUserBioRegistered(dataBio);

  //   if (existBio) {
  //     return res.json({ message: "biodata sudah ada" });
  //   }
  //   UserModel.recordNewdatabio(dataBio);

  //   res.json({ message: "Sukses menambahkan biodata baru!!" });
  // };

  inputGamehistory = async (req, res) => {
    const { idUser } = req.params;
    const userGame = req.body;
    const user = await UserModel.getsingleuserbio(idUser);

    try {
      if (!user) {
        return res.statusCode(404).json({ message: "user id not found" });
      }
      UserModel.recordNewdatahistory(idUser, userGame);
      return res.json({ message: "data recorded" });
    } catch (error) {
      res.statusCode = 400;
      res.json({ message: "there is something wrong" });
    }
  };

  userLogin = async (req, res) => {
    const { username, password } = req.body;
    const dataLogin = await UserModel.verifyLogin(username, password);
    // kalau user valid
    if (dataLogin) {
      // generate JWT
      const token = jwt.sign({ ...dataLogin, role: "player" }, "who knows", {
        expiresIn: "1d",
      });

      // return json web token
      return res.json({ accessToken: token });
    } else {
      return res.json({ message: "credential tidak cocok" });
    }
  };

  getsingleuserhistory = async (req, res) => {
    const { idUser } = req.params;
    const userhistory = await UserModel.getsingleuserhistory(idUser);
    try {
      if (userhistory) {
        return res.json(userhistory);
      } else {
        res.statusCode = 400;
        return res.json({
          message: "history user tidak ditemukan : " + idUser,
        });
      }
    } catch (error) {
      res.statusCode = 400;
      return res.json({ message: "History user tidak ditemukan : " + idUser });
    }
  };

  // create new Game Room
  createGameRoom = async (req, res) => {
    const { roomName, playerOneChoice } = req.body;
    const playerOneId = req.token.id;

    try {
      const roomNameCheck = await UserModel.roomNameCheck(roomName);

      if (roomNameCheck) {
        res.statusCode = 404;
        return res.json({ message: "create other name room" });
      }

      await UserModel.recordGameRoom(roomName, playerOneId, playerOneChoice);

      return res.send({
        message: `Success Record Room with ID : ${playerOneId}`,
      });
    } catch (error) {
      console.log(error);
      res.statusCode = 404;
      return res.json({ message: "unknown error" });
    }
  };

  // mendapatkan data seluruh game room
  getAllGameRooms = async (req, res) => {
    try {
      const allRooms = await UserModel.getAllRooms();
      return res.json(allRooms);
    } catch (error) {
      console.log(error);
      return res.status(404).json({ message: "something error!!" });
    }
  };

  // mendapatkan sebuah data room menggunakan roomid
  getSingleRoom = async (req, res) => {
    // dapatkan id room game dari param
    const { roomId } = req.params;

    try {
      const singleRoom = await UserModel.getSingleGameRoom(roomId);
      if (!singleRoom) {
        return res.status(404).json({ message: "game room not found" });
      }
      return res.json(singleRoom);
    } catch (error) {
      return res.status(404).json({ message: "something error!!" });
    }
  };

  //Update Single Room Game by player two
  updateSingleGameRoom = async (req, res) => {
    const { roomId } = req.params;
    const { playerTwoChoice } = req.body;
    const playerTwoId = req.token.id;

    try {
      // Get Room Detail
      const singleRoomGame = await UserModel.singleRoomGameUpdate(roomId);

      // verifikasi player one
      if (singleRoomGame.playerOneId === playerTwoId) {
        return res.status(400).send("room owner can't fill second choice");
      }

      // verifikasi room already completed
      if (singleRoomGame.status === "Completed") {
        return res.status(400).send("Room is all complete");
      }

      // Input data player two
      await UserModel.updatePlayerTwoData(playerTwoChoice, playerTwoId, roomId);

      //create variable for update the playing
      const choiceOne = singleRoomGame.playerOneChoice;
      const choiceTwo = playerTwoChoice;

      // input varriable to find the winner
      const [playerOneResult, playerTwoResult] = theGameResult(
        choiceOne,
        choiceTwo
      );

      // Input result to database
      await UserModel.updateRoomGameResult(
        playerOneResult,
        playerTwoResult,
        roomId
      );

      return res.json({
        message: " record History Success",
        "playe One Choice": choiceOne,
        "player two Choice": choiceTwo,
        "player One": playerOneResult,
        "player two": playerTwoResult,
      });
    } catch (error) {
      console.log(error);
      return res.status(500).json({ message: "Something Error" });
    }
  };

  //mendapatkan  history game per user
  getSingleUserGameHistory = async (req, res) => {
    try {
      const playerId = req.token.id;

      // find single room detail by ID
      const allSingleUserGameHistory = await UserModel.getRoomDetail(playerId);
      return res.json(allSingleUserGameHistory);
    } catch (error) {
      console.log(error);
      return res.status(500).json({ message: "Something gone wrong" });
    }
  };

  // pertandinga Player vs Computer
  playerVsCom = async (req, res) => {
    const { playerOneChoice } = req.body;
    const playerOne = req.token.id;

    try {
      const roomName = "VS COM";
      const playerTwoChoice = computerChoice(); // ambil dari gameResultValidation
      const [playerOneResult, playerTwoResult] = theGameResult(
        playerOneChoice,
        playerTwoChoice
      );

      await UserModel.playerVsCom(
        roomName,
        playerOneChoice,
        playerTwoChoice,
        playerOne,
        playerOneResult,
        playerTwoResult
      );

      return res.json({
        message: "The Room are successfully created",
        Player: playerOneChoice,
        Computer: playerTwoChoice,
        playerOneResult: playerOneResult,
        playerTwoResult: playerTwoResult,
      });
    } catch (error) {
      console.log(error);
      return res.status(500).json({ message: "Error creating room" });
    }
  };
}

module.exports = new UserController();

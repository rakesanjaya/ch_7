const express = require("express");
const userRouter = express.Router();
const UserController = require("./controller");
const authMiddleware = require("../middleware/authMiddleware");
const protectionMiddleware = require("../middleware/protectionMiddleware");
const userValidation = require("./userValidation");
const schemaValidation = require("../middleware/schemaValidation");

userRouter.get("/users", authMiddleware, UserController.getAllUsers);

userRouter.post(
  "/register",
  userValidation.userRegisterSchema,
  schemaValidation,
  UserController.registerUsers
);

userRouter.post(
  "/login",
  userValidation.userLoginSchema,
  schemaValidation,
  UserController.userLogin
);

userRouter.get(
  "/finduserbio/:idUser",
  authMiddleware,
  UserController.getsingleuserbio
);

userRouter.get(
  "/detail/:idUser",
  authMiddleware,
  protectionMiddleware,
  UserController.getSingleUser
);

userRouter.put(
  "/detail/:idUser",
  authMiddleware,
  protectionMiddleware,
  userValidation.updateUserBioSchema,
  schemaValidation,
  UserController.updateUserBio
);

userRouter.post(
  "/gamehistory/:idUser",
  authMiddleware,
  protectionMiddleware,
  userValidation.inputGamehistorySchema,
  schemaValidation,
  UserController.inputGamehistory
);

userRouter.get(
  "/gamehistory/:idUser",
  authMiddleware,
  protectionMiddleware,
  UserController.getsingleuserhistory
);

// NEW API
// 1. API create GAME ROOM
userRouter.post(
  "/rooms",
  authMiddleware,
  userValidation.createGameRoom,
  schemaValidation,
  UserController.createGameRoom
);

// 2. API untuk mendapatkan semua data game room
userRouter.get("/allRooms", authMiddleware, UserController.getAllGameRooms);

// 3. API untuk mendapatkan detail data salah satu game room
userRouter.get(
  "/singleRoom/:roomId",
  authMiddleware,
  UserController.getSingleRoom
);

// 4. API untuk mengupdate single room game
userRouter.put(
  "/updateRoom/:roomId",
  authMiddleware,
  UserController.updateSingleGameRoom
);

// 5. API untuk mendapatkan history setiap user berdasarkan id dari token id
userRouter.get(
  "/allHistory",
  authMiddleware,
  UserController.getSingleUserGameHistory
);

// 6. API player vs com
userRouter.post("/vsCom", authMiddleware, UserController.playerVsCom);

module.exports = userRouter;

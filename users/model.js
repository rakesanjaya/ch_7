const md5 = require("md5");
const db = require("../db/models");
const { Op } = require("sequelize");

const daftarUser = [];

class UserModel {
  // dapatkan semua data
  getAllUser = async () => {
    const daftarUser = await db.User.findAll({
      include: [db.UserBio, db.GameHistory],
    });
    // SELECT * FROM users
    return daftarUser;
  };

  // mencari 1 data(single user)
  getSingleUser = async (idUser) => {
    return await db.User.findOne({ where: { id: idUser } });
  };

  getsingleuserbio = async (idUser) => {
    return await db.User.findOne({
      include: [db.UserBio],
      where: { id: idUser },
    });
  };
  updateUserBio = async (idUser, fullname, PhoneNumber, Address) => {
    return await db.UserBio.update(
      { fullname: fullname, PhoneNumber: PhoneNumber, Address: Address },
      { where: { User_id: idUser } }
    );
  };
  isUserBioExist = async (idUser) => {
    const existData = await db.UserBio.findOne({
      where: {
        User_id: idUser,
      },
    });
    if (existData) {
      return true;
    } else {
      return false;
    }
  };

  createUserBio = async (idUser, fullname, PhoneNumber, Address) => {
    return await db.UserBio.create({
      fullname,
      PhoneNumber,
      Address,

      User_id: idUser,
    });
  };

  //   method check username terregister atau tidak

  isUserRegistered = async (dataRequest) => {
    const existData = await db.User.findOne({
      where: {
        [Op.or]: [
          { username: dataRequest.username },
          { email: dataRequest.email },
        ],
      },
    });
    if (existData) {
      return true;
    } else {
      return false;
    }
  };

  // method check data bio
  // isUserBioRegistered = async (dataBio) => {
  //   const existBio = await db.UserBio.findOne({
  //     where: {
  //       [Op.or]: [
  //         { fullname: dataBio.fullname },
  //         { PhoneNumber: dataBio.PhoneNumber },
  //         { Address: dataBio.Address },
  //         { User_id: dataBio.User_id },
  //       ],
  //     },
  //   });
  //   if (existBio) {
  //     return true;
  //   } else {
  //     return false;
  //   }
  // };

  // userSudahpernahmain = async (dataHistory) => {
  //   const sudahpernahmain = await db.GameHistory.findOne({
  //     where: {
  //       [Op.or]: [{ status: dataHistory.status }],
  //     },
  //   });
  //   if (sudahpernahmain) {
  //     return true;
  //   } else {
  //     return false;
  //   }
  // };

  // method record data

  recordNewdata = (dataRequest) => {
    // INSERT INTO table () values()
    db.User.create({
      username: dataRequest.username,
      email: dataRequest.email,
      password: md5(dataRequest.password),
      fullname: dataRequest.fullname,
    });
  };

  recordNewdatahistory = (idUser, userGame) => {
    db.GameHistory.create({
      User_id: idUser,
      gameName: userGame.gameName,
      status: userGame.status,
    });
  };

  // mendapatkan data game history 1 user
  getsingleuserhistory = async (idUser) => {
    return await db.User.findOne({
      include: [db.GameHistory],
      where: { id: idUser },
    });
  };

  // login
  verifyLogin = async (username, password) => {
    const dataUser = await db.User.findOne({
      where: { username: username, password: md5(password) },
      attributes: { exclude: "password" },
      raw: true,
    });

    return dataUser;
  };

  // FOR NEW API
  // validasi nama room
  roomNameCheck = async (roomName) => {
    try {
      return await db.gameRoom.findOne({
        where: { roomName: roomName },
      });
    } catch (error) {
      throw new Error(
        `Something wrong, Failed to check Room Name : ${error.message}`
      );
    }
  };

  // create Game Room
  recordGameRoom = async (roomName, playerOneId, playerOneChoice) => {
    try {
      const gameRoom = await db.gameRoom.create({
        roomName: roomName,
        playerOneId: playerOneId,
        playerOneChoice: playerOneChoice,
        roomStatus: "available",
      });
      return gameRoom;
    } catch (error) {
      throw new Error(
        `Something wrong, Failed to create new Room : ${error.message}`
      );
    }
  };
  // mendapatkan ALL Rooms data
  getAllRooms = async () => {
    try {
      const roomList = await db.gameRoom.findAll({
        include: [
          {
            model: db.User,
            as: "playerOne",
            attributes: ["id", "username"],
          },
          {
            model: db.User,
            as: "playerTwo",
            attributes: ["id", "username"],
          },
        ],
      });
      return roomList;
    } catch (error) {
      throw new Error(`Failed to get all Rooms Data : ${error.message}`);
    }
  };

  // mendapatkan detail dari single game room
  getSingleGameRoom = async (roomId) => {
    try {
      const singleRoomDetail = await db.gameRoom.findOne({
        where: { id: roomId },
        include: [
          {
            model: db.User,
            as: "playerOne",
            attributes: ["id", "username"],
          },
          {
            model: db.User,
            as: "playerTwo",
            attributes: ["id", "username"],
          },
        ],
        attributes: { exclude: ["playerTwoChoice", "playerOneChoice"] },
        raw: true,
      });
      return singleRoomDetail;
    } catch (error) {
      throw new Error(`Failed to get room game detail : ${error.message}`);
    }
  };

  // detail room game for update
  singleRoomGameUpdate = async (roomId) => {
    try {
      const roomSingleList = await db.gameRoom.findOne({
        where: { id: roomId },
        include: [
          {
            model: db.User,
            as: "playerOne",
            attributes: ["id", "username"],
          },
          {
            model: db.User,
            as: "playerTwo",
            attributes: ["id", "username"],
          },
        ],

        raw: true,
      });
      return roomSingleList;
    } catch (error) {
      throw new Error(`Failed to get room detail : ${error.message}`);
    }
  };

  // update data player two
  updatePlayerTwoData = async (playerTwoChoice, playerTwoId, roomId) => {
    try {
      const roomIdInt = parseInt(roomId);
      const playerTwoUpdate = await db.gameRoom.update(
        {
          playerTwoChoice: playerTwoChoice,
          playerTwoId: playerTwoId,
        },
        { where: { id: roomIdInt } }
      );
      return playerTwoUpdate;
    } catch (error) {
      throw new Error(`Failed to input player2 detail : ${error.message}`);
    }
  };

  // Update Room result from player two choice
  updateRoomGameResult = async (playerOneResult, playerTwoResult, roomId) => {
    const roomIdInt = parseInt(roomId);
    // console.log(roomIdInt);
    try {
      const playerTwoUpdate = await db.gameRoom.update(
        {
          playerOneResult: playerOneResult,
          playerTwoResult: playerTwoResult,
          roomStatus: "Completed",
        },
        { where: { id: roomIdInt } }
      );
      return playerTwoUpdate;
    } catch (error) {
      throw new Error(`Failed to update game room data : ${error.message}`);
    }
  };

  //mendapatkan single user all game history
  getRoomDetail = async (playerId) => {
    try {
      const roomList = await db.gameRoom.findAll({
        attributes: [
          "roomName",
          "updatedAt",
          "playerOneId",
          "playerTwoId",
          "playerOneResult",
          "playerTwoResult",
          "roomStatus",
        ],
        where: {
          [Op.or]: [{ playerOneId: playerId }, { playerTwoId: playerId }],
        },
        raw: true,
      });

      return roomList;
    } catch (error) {
      throw new Error(`Failed to retrieve all game history : ${error.message}`);
    }
  };

  playerVsCom = async (
    roomName,
    playerOneChoice,
    playerTwoChoice,
    playerOne,
    playerOneResult,
    playerTwoResult
  ) => {
    try {
      const gameRoomVsCom = await db.gameRoom.create({
        roomName,
        playerOneChoice,
        playerOneId: playerOne,
        playerTwoChoice,
        playerTwoId: 2, // make playerTwoId as ID Computer
        playerOneResult,
        playerTwoResult,
        statusRoom: "Completed", // change status room to "Completed"
      });
      return gameRoomVsCom;
    } catch (error) {
      throw new Error(
        `Failed to create game room with computer : ${error.message}`
      );
    }
  };
}
module.exports = new UserModel();

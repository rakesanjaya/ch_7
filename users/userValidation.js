const { body } = require("express-validator");

const userRegisterSchema = [
  body("username")
    .notEmpty()
    .withMessage("please enter the username")
    .isString()
    .withMessage("must be character"),

  body("email")
    .notEmpty()
    .withMessage("please enter the email")
    .isEmail()
    .withMessage("must be correct email"),

  body("password")
    .notEmpty()
    .withMessage("please enter the password")
    .isLength({
      min: 5,
      max: 15,
    })
    .withMessage("password must be 5 character and max 15 character"),
];

const userLoginSchema = [
  body("username")
    .notEmpty()
    .withMessage("please enter the username")
    .isString()
    .withMessage("must be character"),

  body("email")
    .notEmpty()
    .withMessage("please enter the email")
    .isEmail()
    .withMessage("must be correct email"),

  body("password")
    .notEmpty()
    .withMessage("please enter the password")
    .isLength({
      min: 5,
      max: 15,
    })
    .withMessage("password must be 5 character and max 15 character"),
];

const updateUserBioSchema = [
  body("fullname")
    .notEmpty()
    .withMessage("please enter the fullname")
    .isString()
    .withMessage("must be character"),

  body("PhoneNumber")
    .notEmpty()
    .withMessage("please enter the phone number")
    .isInt()
    .withMessage("must be number"),

  body("Address")
    .notEmpty()
    .withMessage("please enter your address")
    .isString()
    .withMessage("must be character"),
];

const inputGamehistorySchema = [
  body("gameName")
    .notEmpty()
    .withMessage("please enter gameName")
    .isString()
    .withMessage("must be character"),

  body("status")
    .notEmpty()
    .withMessage("please enter the status")
    .isString()
    .withMessage("must be character"),
];

const createGameRoom = [
  body("roomName")
    .notEmpty()
    .withMessage("Please enter the room Name")
    .isString()
    .withMessage("Must be character"),
  body("playerOneChoice")
    .notEmpty()
    .withMessage("Please enter your choice")
    .custom((value) => {
      const validChoices = ["rock", "paper", "scissor"];
      if (!validChoices.includes(value)) {
        throw new Error("Invalid choice, should be rock, paper, or scissor");
      }
      return true;
    }),
];

module.exports = {
  userRegisterSchema,
  userLoginSchema,
  updateUserBioSchema,
  inputGamehistorySchema,
  createGameRoom,
};
